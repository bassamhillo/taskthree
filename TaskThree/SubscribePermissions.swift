//
//  SubscribePermissions.swift
//  TaskThree
//
//  Created by Julia on 7/19/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation
import SPPermissions

class SubscribePermissions {
    
    var viewController:UIViewController?
    
    init(viewController:UIViewController) {
        self.viewController = viewController
    }
    
    func showPermissionsDialog(headerText:String, titleText:String, footerText:String) {
        
        let permissions = [.notification].filter { !($0 as SPPermission).isAuthorized }
        if permissions.isEmpty {
            return
        } else {
            let controller = SPPermissions.dialog([.notification])
            
            controller.delegate = viewController as! SPPermissionsDelegate
    
           // Ovveride texts in controller
           controller.titleText = titleText
           controller.headerText = headerText
           controller.footerText = footerText

           // Always use this method for present
           if viewController != nil {
               controller.present(on: viewController!)
           }
        }
    }
}
