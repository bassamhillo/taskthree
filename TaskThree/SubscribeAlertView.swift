//
//  SubscribeAlertView.swift
//  TaskThree
//
//  Created by Julia on 7/19/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation
import SCLAlertView

protocol showSubscribedInfo {
    func showInfo()
}

class SubscribeAlertView{
    
    let subscribeUtil = SubscribeUtil()
    let subscribeNotification = SubscribeNotification()
    var delegate:showSubscribedInfo?
    var viewController:UIViewController?
    
    init(viewController:UIViewController) {
        delegate = viewController as? showSubscribedInfo
        self.viewController = viewController
    }
    func showAlert(_ nameTextFieldTxt:String?,_ emailTFText:String?,_ invalidEmail:Bool?,_ invalidName:Bool?)  {

        // Hide default button
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        
        let alertView = SCLAlertView(appearance: appearance)
        
        
        let userNameTF = alertView.addTextField("Name")
        let emailTF = alertView.addTextField("Email")
                
        
        if nameTextFieldTxt != nil {
            userNameTF.text = nameTextFieldTxt!
        }
                
        if emailTFText != nil {
            emailTF.text = emailTFText!
        }

        
        if invalidEmail != nil && invalidEmail! {
            emailTF.text = ""
            emailTF.placeholder = "Invalid email"
        }

        if invalidName != nil && invalidName! {
            userNameTF.text = ""
            userNameTF.placeholder = "Invalid name"
        }

        
        alertView.addButton("Subscribe") {
            let name = userNameTF.text
            let email = emailTF.text
            
            var emailError = false
            var nameError = false
            
            if email == nil || email == ""  || !self.isValidEmail(email!) {
                emailError = true
            }
            
            if name == nil || name == "" || !self.isValidName(name!) {
                nameError = true
            }
            
            if emailError || nameError {
                SubscribeAlertView(viewController: self.viewController!).showAlert(name!, email!, emailError, nameError)
                return
            }
            
            self.subscribeNotification.removeSubscribeRequest()
            self.saveEmailAndName(email!, name!)
            self.delegate?.showInfo()

        }
        alertView.addButton("Cancel") {
            self.subscribeNotification.startNotification()
        }
        
        alertView.showEdit("Subscripe", subTitle: "Enter name and email to subscribe")

    }
    
    // Save the email and password in user defaults
    func saveEmailAndName(_ email:String, _ name:String) {
        let userDefaults = UserDefaults.standard
        
        let emailKey = subscribeUtil.emailKey
        let nameKey = subscribeUtil.nameKey
        
        userDefaults.set(email, forKey: emailKey)
        userDefaults.set(name, forKey: nameKey)
    }
    
    // Check if email is valid
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    
    
    // Check if email is valid
    func isValidName(_ name: String) -> Bool {
        let nameRegEx = "^\\w{3,18}$"

        let namePred = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return namePred.evaluate(with: name)
    }

}
