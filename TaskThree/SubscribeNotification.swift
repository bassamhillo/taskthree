//
//  SubscribeNotification.swift
//  TaskThree
//
//  Created by Julia on 7/16/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation
import UserNotifications

class SubscribeNotification {

    let content = UNMutableNotificationContent()
    var notificationCenter = UNUserNotificationCenter.current()
    let identifire = "Subscribe local notification"
    
    // Stop the subscribe notification
    func removeSubscribeRequest() {
        notificationCenter.removePendingNotificationRequests(withIdentifiers: [self.identifire])
    }

    // Enable the subscribe notification every 1 minute
    func startNotification() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
                if !didAllow {
                    print("User has declined notifications")
                }
        }

        content.title = "Subscribe"
        content.body = "You are not subscribed"
        content.sound = .default
        content.badge = 1
        
        let date = Date(timeIntervalSinceNow: 5)
        let triggerDaily = Calendar.current.dateComponents([.second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
        
        let request = UNNotificationRequest(identifier: self.identifire, content: content, trigger: trigger)

        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
        
    }
}
