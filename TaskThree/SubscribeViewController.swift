//
//  ViewController.swift
//  TaskThree
//
//  Created by Julia on 7/16/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit
import SPPermissions

class SubscribeViewController: UIViewController {
    
    @IBOutlet weak var subscribeTV: UITextView!
    @IBOutlet weak var titleView: UIView!
    
    let subscribeUtil = SubscribeUtil()
    var subscribePermissions:SubscribePermissions?
    var subscribeAlertView:SubscribeAlertView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleView.isHidden = true
        // Permission dialog init
        self.subscribePermissions =  SubscribePermissions(viewController: self)
        self.subscribeAlertView = SubscribeAlertView(viewController: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let permissions = [.notification].filter { !($0 as SPPermission).isAuthorized }
        if permissions.isEmpty {
            self.showSubscribedInfo()
        }
        
        self.subscribePermissions!.showPermissionsDialog(headerText: "Permissions", titleText: "App needs notifications Permissions", footerText: "")
    }
    
    func showSubscribedInfo() {
        let userDefaults = UserDefaults()
                
        let emailKey = subscribeUtil.emailKey
        let email = userDefaults.string(forKey: emailKey)
        
        if email != nil && email != "" {
            titleView.isHidden = false
            subscribeTV.text = "You have been subscribed with Email '\(email!)'"
        } else {
            subscribeAlertView!.showAlert(nil, nil, nil, nil)
        }

    }
    
}

extension SubscribeViewController:SPPermissionsDelegate {
    func didAllow(permission: SPPermission) {
        self.showSubscribedInfo()
    }
}

extension SubscribeViewController:showSubscribedInfo {
    func showInfo() {
        self.showSubscribedInfo()
    }
}




